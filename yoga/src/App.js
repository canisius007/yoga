import './App.css';
import About from './Components/About/About';
import Event from './Components/Event/Event';
import Home from './Components/Home/Home';
import Navbar from './Components/Navbar/Navbar';
import Title from './Components/Title/Title';
import Course from './Components/Course/Course'; // Import Course here
import { Route, BrowserRouter as Router, Routes,useLocation } from 'react-router-dom';
import Course1 from './Components/Course/Course1/Course1'; // Import Course1 here
import Course2 from './Components/Course/Course2/Course2';
import Course3 from './Components/Course/Course3/Course3';
import Course4 from './Components/Course/Course4/Course4';


function App() {
  return (
    <Router>
      <div>
        <MainContent />
      </div>
    </Router>
  );
}

function MainContent() {
  const location = useLocation();
  const isCourse1 = location.pathname === '/course1';
  const isCourse2 = location.pathname === '/course2';
  const isCourse3 = location.pathname === '/course3';
  const isCourse4 = location.pathname === '/course4';

  return (
    <div>
      {!isCourse1 && !isCourse2 && !isCourse3 && !isCourse4 && <Navbar />}
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/course1" element={<Course1 />} />
        <Route path="/course2" element={<Course2 />} />
        <Route path="/course3" element={<Course3 />} />
        <Route path="/course4" element={<Course4 />} />
        <Route path="/course" element={<Course />} />
        {/* Add routes for other courses if needed */}
      </Routes>
      {!isCourse1 && !isCourse2 && !isCourse3 && !isCourse4 && (
        <>
        <About/>
        <div className="container">
          <Title title="Our Courses" subtitle="What we offer" />
          <Course />
          <Title title="Events" subtitle="what we conducted" />
          <Event />
        </div>
        </>
      )}
    </div>
  );
}


export default App;
