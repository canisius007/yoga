import React from 'react';
import './About.css';
import aboutImage from '../../assets/About1.jpg';


const About = () => {
  return (
    <div className='about' id='about' > {/* Ensure component has full height */}
      <div className="about-left">
        <img src={aboutImage} className='about-img' alt="About" />
      </div>
      <div className="about-right">
        <h1>About Us</h1>
        <p>
          Welcome to Salman Yoga, where the journey to holistic well-being begins. At Salman Yoga, we believe in the transformative power of yoga, meditation, and mudras to nourish the body, calm the mind, and uplift the spirit.
        </p>
        <div>
          <p><strong>Our Mission:</strong></p>
          <p>Salman Yoga is dedicated to making the ancient wisdom of yoga accessible to all, fostering a community committed to personal growth, balance, and mindfulness. We aim to guide individuals on a path of self-discovery, promoting physical vitality, mental clarity, and emotional serenity.</p>
          <p><strong>What Sets Us Apart:</strong></p>
          <p>
            <span>Comprehensive Approach:</span> Salman Yoga embraces a holistic approach to well-being, offering a diverse range of practices, including yoga asanas, meditation techniques, and powerful mudras.
            <br/>
            <span>Experienced Instructors:</span> Our passionate and experienced instructors are committed to creating a supportive and inclusive environment, guiding practitioners of all levels on their unique wellness journey.
            <br/>
            <span>Rooted in Tradition, Tailored for Today:</span> While rooted in the rich traditions of yoga, our teachings are thoughtfully adapted to meet the needs of modern lifestyles, ensuring relevance and effectiveness
          </p>
        </div>
      </div>
    </div>
  );
};

export default About;
