import React from 'react'
import './Event.css'
import next_icon from '../../assets/next-icon.png'
import back_icon from '../../assets/back-icon.png'
import event_1 from '../../assets/event_1.jpg'
import event_2 from '../../assets/event_2.jpg'
import event_3 from '../../assets/event_3.jpg'
import event_4 from '../../assets/event_4.jpg'

const Event = () => {
    return (
        <div className='event'>
            <img src={next_icon} alt='next' className='next-btn' />
            <img src={back_icon} alt='next' className='back-btn' />
            <div className='slider'>
                <ul>
                    <li>
                        <div className='slide'>
                            <div className='event-info'>
                                <img src={event_1} alt='event1' />
                                <div>
                                    <h3>Yoga Hamsa</h3>
                                </div>
                            </div>
                            <p>Yoga is a group of physical, mental, and spiritual practices or disciplines
                                which originated in ancient India and aim to control (yoke) and still the mind,
                            </p>
                        </div>
                    </li>
                    <li>
                        <div className='slide'>
                            <div className='event-info'>
                                <img src={event_2} alt='event1' />
                                <div>
                                    <h3>Yoga Hamsa</h3>
                                </div>
                            </div>
                            <p>Yoga is a group of physical, mental, and spiritual practices or disciplines
                                which originated in ancient India and aim to control (yoke) and still the mind,
                            </p>
                        </div>
                    </li>
                    <li>
                        <div className='slide'>
                            <div className='event-info'>
                                <img src={event_3} alt='event1' />
                                <div>
                                    <h3>Yoga Hamsa</h3>
                                </div>
                            </div>
                            <p>Yoga is a group of physical, mental, and spiritual practices or disciplines
                                which originated in ancient India and aim to control (yoke) and still the mind,
                            </p>
                        </div>
                    </li>
                    <li>
                        <div className='slide'>
                            <div className='event-info'>
                                <img src={event_4} alt='event1' />
                                <div>
                                    <h3>Yoga Hamsa</h3>
                                </div>
                            </div>
                            <p>Yoga is a group of physical, mental, and spiritual practices or disciplines
                                which originated in ancient India and aim to control (yoke) and still the mind,
                            </p>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    )
}

export default Event