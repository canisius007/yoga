import React, { useEffect, useState } from 'react';
import { Link as LinkS } from 'react-scroll';
import './Navbar.css';

const Navbar = () => {
  const [sticky, setSticky] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      window.scrollY > 50 ? setSticky(true) : setSticky(false);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <nav className={`container ${sticky ? 'darkNav' : ''}`}>
      <h1>Salman Yoga</h1>
      <ul>
        <li>
          <LinkS to='home' smooth={true} offset={0} duration={500}>Home</LinkS>
        </li>
        <li>
          <LinkS to='about' smooth={true} offset={0} duration={500}>About Us</LinkS>
        </li>
        <li>
          <LinkS to='courses' smooth={true} offset={0} duration={500}>Courses</LinkS>
        </li>
        <li>
          <LinkS to='event' smooth={true} offset={0} duration={500}>Events</LinkS>
        </li>
        <li>
          <LinkS to='home' smooth={true} offset={0} duration={500}>Gallery</LinkS>
        </li>
        <li>
          <LinkS to='home' smooth={true} offset={0} duration={500} className='btn'>Contact us</LinkS>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
