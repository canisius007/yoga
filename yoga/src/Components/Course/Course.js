import React from 'react';
import { useNavigate } from 'react-router-dom';
import Course1img from '../../assets/Course1ashtanga yoga.jpg';
import Course2img from '../../assets/Course2AnimalPose.jpg';
import Course3img from '../../assets/Course3Mudra.jpg';
import Arrow from '../../assets/white-arrow.png';
import './Course.css';

const Course = () => {
    const navigate = useNavigate();

    const handleNavigate = () => {
        navigate('/course1');
    };
    const handleNavigate1 = () => {
        navigate('/course2');
    };
    const handleNavigate2 = () => {
        navigate('/course3');
    };
    
    const handleNavigate3 = () => {
        navigate('/course4');
    };

    return (
        <div className='courses' id='courses'>
            <div className='course'>
                <img src={Course1img} alt="Ashtanga Yoga"></img>
                <div className='caption'>
                    <p>Ashtanga Yoga</p>
                    <button className='btn1' onClick={handleNavigate}>
                        <img src={Arrow} alt="Ashtanga Yoga" />
                    </button>
                </div>
            </div>

            <div className='course'>
                <img src={Course2img} alt="Animal Movement"></img>
                <div className='caption'>
                    <p>Animal Movement</p>
                    <button className='btn1' onClick={handleNavigate1}>
                        <img src={Arrow} alt="Ashtanga Yoga" />
                    </button>
                    {/* <LinkR to="/course2" className='btn1'><img src={Arrow} alt="Animal Movement" /></LinkR> */}
                </div>
            </div>
            <div className='course'>
                <img src={Course3img} alt="Mudras"></img>
                <div className='caption'>
                    <p>Mudras</p>
                    <button className='btn1' onClick={handleNavigate2}>
                        <img src={Arrow} alt="Ashtanga Yoga" />
                    </button>
                    {/* <LinkR to="/course3" className='btn1'><img src={Arrow} alt="Mudras" /></LinkR> */}
                </div>
            </div>
            <div className='course'>
                <img src={Course3img} alt="Meditation"></img>
                <div className='caption'>
                    <p>Meditation</p>
                    <button className='btn1' onClick={handleNavigate3}>
                        <img src={Arrow} alt="Ashtanga Yoga" />
                    </button>
                    {/* <LinkR to="/meditation" className='btn1'><img src={Arrow} alt="Meditation" /></LinkR> */}
                </div>
            </div>
        </div>
    );
};

export default Course;
