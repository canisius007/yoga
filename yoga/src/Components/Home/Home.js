import React from 'react'

import './Home.css'
import Arrow from '../../assets/white-arrow.png'

const Home = () => {
  return (
    <div className='home container' id='home'>
<div className='home-text '>
  <h1>
  Yoga for Body, Mind, and Soul
  </h1>
  <p>Yoga is a powerful tool for self-discovery and personal growth.
  It promotes overall well-being and harmony, creating a sense of balance and peace within.
  </p>
  <button className='btn'>Explore More <img src={Arrow} alt="" /></button>
</div>
    </div>
  )
}

export default Home